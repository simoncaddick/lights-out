﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightsOut
{
    public class GamePlay
    {
        private List<Bulb> _bulbs;
        private int[] _entry;
        private int _bulbPressed;
        private bool _exit, _valid;
        public GamePlay(List<Bulb> gameBulbs)
        {
            _exit = false;
            _bulbs = gameBulbs;
            Console.WriteLine("Wecome to LightsOut");
        }

        public void Play()
        {
            Console.Clear();
            DisplayLights(_bulbs);
            Instructions();
            CheckEntry();
            if(_valid == false)
            {
                Play();
            }
            else if(_exit == false)
            {
                SwitchBulbs(_bulbs);
                CheckLights(_bulbs);
            }
        }

        public void CheckLights(List<Bulb> bulbs)
        {
            int lightsOn = 0;
            foreach(Bulb bulb in bulbs)
            {
                if (bulb.LightOn)
                {
                    lightsOn++;
                }
            }
            if (lightsOn > 0)
            {
                Play();
            }
            else
            {
                Console.WriteLine("You have completed the game. \n");
            }
        }

        public void SwitchBulbs(List<Bulb> bulbs)
        {
            _bulbPressed = (_entry[0] + (5 * (_entry[1] )));
            Console.WriteLine("Bulb selected = {0}", _bulbPressed);
            bulbs[_bulbPressed].ToggleLight();
            Left(_entry);
            Right(_entry);
            Up(_entry);
            Down(_entry);
        }

        public bool Left(int[] coordinates)
        {
            if(coordinates[0]%5 > 0 && _bulbPressed > 0)
            {
                _bulbs[_bulbPressed - 1].ToggleLight();
                return true;
            }
            return false;
        }

        private bool Right(int[] coordinates)
        {
            if (coordinates[0] % 5 < 4 && _bulbPressed < 24)
            {
                _bulbs[_bulbPressed + 1].ToggleLight();
                return true;
            }
            return false;
        }

        private bool Up(int[] coordinates)
        {
            if (coordinates[1] % 5 > 0 && _bulbPressed > 4)
            {
                _bulbs[_bulbPressed - 5].ToggleLight();
                return true;
            }
            return false;
        }

        private bool Down(int[] coordinates)
        {
            if (coordinates[1] % 5 < 4 && _bulbPressed < 20)
            {
                _bulbs[_bulbPressed + 5].ToggleLight();
                return true;
            }
            return false;
        }

        public void CheckEntry()
        {
            int addition = -1;
            int multiplier = -1;
            string entry;
            entry = Console.ReadLine();
            if(entry.Length > 2 || entry.Length < 2)
            {
                if(string.Equals("exit", entry))
                {
                    _exit = true;
                }
                else
                {
                    Console.WriteLine("Entry is invalid, please try again");
                    _valid = false;
                }
            }
            else
            {
                char firstChar = entry[0];
                char secondChar = entry[1];
                if (_exit != true)
                {

                    if (!char.IsNumber(firstChar))
                    {
                        switch (firstChar)
                        {
                            case 'A':
                            case 'a':
                                addition = 0;
                                break;
                            case 'B':
                            case 'b':
                                addition = 1;
                                break;
                            case 'C':
                            case 'c':
                                addition = 2;
                                break;
                            case 'D':
                            case 'd':
                                addition = 3;
                                break;
                            case 'E':
                            case 'e':
                                addition = 4;
                                break;
                            default:
                                Console.WriteLine("Invalid First entry, please try again using letters from A-E");
                                break;
                        }
                    }
                    if (char.IsNumber(secondChar))
                    {
                        switch (secondChar)
                        {
                            case '1':
                                multiplier = 0;
                                break;
                            case '2':
                                multiplier = 1;
                                break;
                            case '3':
                                multiplier = 2;
                                break;
                            case '4':
                                multiplier = 3;
                                break;
                            case '5':
                                multiplier = 4;
                                break;
                            default:
                                Console.WriteLine("Second entry is not valid, please enter a value between 0-4");
                                break;
                        }
                    }
                    if (addition == -1 || multiplier == -1)
                    {
                        _valid = false;
                    }
                    else
                    {
                        _entry = new int[2] { addition, multiplier };
                        _valid = true;
                    }
                    
                }
            }
        }

        // repeat instructions as the console scrolls
        public void Instructions()
        {
            Console.WriteLine("Enter the reference LETTER then NUMBER eg. A1, then hit Enter");
            Console.WriteLine("Enter 'exit' to leave the game.");
        }

        //Console display of the grid of lights
        public void DisplayLights(List<Bulb> bulbs)
        {
            int index = 1;
            int row = 1;
            Console.WriteLine("   || A | B | C | D | E");
            Console.WriteLine("=======================");
            Console.Write(" {0} ||", row);
            for(int i = 0; i < bulbs.Count; i++)
            {
                int on = Convert.ToInt32(bulbs[i].LightOn);
                // Separate onto a separate line
                if(index%5 == 0)
                {
                    Console.WriteLine(" {0} ", on);
                    row++;
                    if(row <= 5)
                    {
                        Console.Write(" {0} ||", row);
                    }
                }
                else
                {
                    Console.Write(" {0} |", on);
                }
                index++;
            }
        }
    }
}
