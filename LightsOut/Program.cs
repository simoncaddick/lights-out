﻿using System;

namespace LightsOut
{
    class Program
    {
        static void Main(string[] args)
        {
            Introduction();
        }

        private static void Introduction()
        {
            Console.Clear();
            Console.WriteLine("Welcome to Lights Out, to start a new game please hit 1 to run the Test program, ");
            Console.WriteLine("2 to randomly generate a new game or to exit hit any other key.");
            Console.WriteLine("The aim of the game is to Turn all lights off");
            Console.WriteLine("Selecting a light will toggle it and the four surrounding lights");
            string choice = Console.ReadLine();
            if (choice == "1")
            {
                NewGame(true);
                Introduction();
            }
            else if (choice == "2")
            {
                NewGame(false);
                Introduction();
            }
            else { }

        }

        private static void NewGame(bool test)
        {
            Game newGame = new Game();
            newGame.GenerateNewGame(test);
        }
    }
}
