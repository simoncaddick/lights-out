﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightsOut
{
    public class Bulb
    {
        public Bulb()
        {
        }

        public bool LightOn { get; set; }

        public void TurnOn()
        {
            LightOn = true;
        }

        public void TurnOff()
        {
            LightOn = false;
        }

        public void ToggleLight()
        {
            if (LightOn)
            {
                TurnOff();
            }
            else
            {
                TurnOn();
            }
        }
    }
}
