﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightsOut
{
    public class Game
    {
        private static int _gameSize = 5;
        private static int _numberOfBulbs = _gameSize * _gameSize;
        private List<Bulb> _gameLights;
        private Random isOn;
        private GamePlay _game;

        public Game()
        {
            isOn = new Random();
        }

        public void GenerateNewGame(bool test)
        {
            _gameLights = new List<Bulb>();
            for (int i = 0; i < _numberOfBulbs; i++)
            {
                Bulb bulb = new Bulb();
                _gameLights.Add(bulb);
            }
            if (!test)
            {
                // Randomly Generated
                for (int i = 0; i < _numberOfBulbs; i++)
                {
                    bool bulbOn = RandomOnOff();
                    _gameLights[i].LightOn = bulbOn;
                }                    
            }
            else
            {
                int[] testBulbs = new int[] { 1, 3, 5, 6, 8, 9, 10, 11, 20, 24 };
                foreach(int i in testBulbs)
                {
                    _gameLights[i].TurnOn();
                }

            }
            _game = new GamePlay(_gameLights);
            _game.Play();
        }
        

        public bool RandomOnOff()
        {
            int randomVal = isOn.Next(1, 100);
            if(randomVal%4 == 0)
            {
                return false;
            }
            return true;
        }
    }
}
