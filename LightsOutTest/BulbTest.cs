using LightsOut;
using NUnit.Framework;

namespace LightsOutTest
{
    [TestFixture]
    public class BulbTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TurnOnTest()
        {
            Bulb bulb = new Bulb();
            bulb.LightOn = false;
            bulb.TurnOn();
            bool isOn = bulb.LightOn;
            Assert.AreEqual(true, isOn);
        }

        [Test]
        public void TurnOffTest()
        {
            Bulb bulb = new Bulb();
            bulb.LightOn = true;
            bulb.TurnOff();
            bool isOn = bulb.LightOn;
            Assert.AreEqual(false, isOn);
        }

        [Test]
        public void ToggleLightTest()
        {
            Bulb offBulb = new Bulb();
            Bulb onBulb = new Bulb();
            onBulb.LightOn = true;
            offBulb.LightOn = false;
            onBulb.ToggleLight();
            offBulb.ToggleLight();
            bool isOff = onBulb.LightOn;
            bool isOn = offBulb.LightOn;
            Assert.AreEqual(true, isOn);
            Assert.AreEqual(false, isOff);
        }
    }
}